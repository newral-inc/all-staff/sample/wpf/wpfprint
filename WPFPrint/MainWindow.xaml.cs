﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFPrint
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            // 印刷ダイアログを表示します。
            var printDialog = new PrintDialog();
            var result = printDialog.ShowDialog();

            // 印刷ボタン以外が押下された場合、処理を終了します。
            if (!result.HasValue || !result.Value) return;

            // 印刷データを生成します。
            var textBlock = new TextBlock();
            textBlock.Text = "テスト";
            textBlock.FontSize = 16;

            var canvas = new Canvas();
            Canvas.SetTop(textBlock, 100);
            Canvas.SetLeft(textBlock, 100);
            canvas.Children.Add(textBlock);

            var page = new FixedPage();
            page.Children.Add(canvas);

            // 印刷します。
            var queue = printDialog.PrintQueue;
            var writer = PrintQueue.CreateXpsDocumentWriter(queue);
            writer.Write(page);
        }
    }
}
